#!/usr/bin/env python3

import socket
import sys
import re
import os


s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#s.connect((sys.argv[1],int(sys.argv[2])))
arg1=sys.argv[1]
l=['301','302','303','307','308']
dic={}

m=re.match('^https',arg1)
if m:
    s.close()
    sys.exit(0)
m=re.match('http:\/\/www.([a-z0-9A-Z]+)([\.])([a-z]+)',arg1)
if not m:
    s.close()
    sys.exit(1)

sr=arg1[7:]
r=''
for elem in sr:
    if elem!='/':
        r=r+elem
    else:
        break

s.connect((r, 80))
f=s.makefile('rwb')
p=os.fork()
f.write(str('GET '+str(arg1)+' HTTP/1.1\r\n').encode('ASCII'))
f.write(('Host: '+str(r)+'\r\n').encode('ASCII'))
f.write('\r\n'.encode('ASCII'))
f.flush()

while True:
    
    status=f.readline()
    status=status.decode('ASCII')
    stat=status
    status=status.split()
    if len(status)>=2:
        if status[1]=='200':
            print(stat)
            dic={}
            while True:
                line=f.readline()
                line=line.decode('ASCII')
                print(line)
                le=line.split()
                if len(le)>=2:
                    dic[str.lower(le[0][:-1])]=le[1]
                if line=='\r\n':
                    break
            break
        
        elif status[1] in l:
            while True:
                line=f.readline()
                line=line.decode('ASCII')
                le=line.split()
                if len(le)>=2:
                    dic[str.lower(le[0][:-1])]=le[1]
                if line=='\r\n':
                    if 'location' in dic:
                        f.read(int(dic['content-length']))
                        url=str(dic['location'])
                        m=re.match('^https',url)
                        if m:
                            print('len http')
                            s.close()
                            sys.exit(1)
                        f.write(str('GET '+str(dic['location'])+' HTTP/1.1\r\n').encode('ASCII'))
                        f.write(('Host: '+str(r)+'\r\n').encode('ASCII'))
                        f.write('\r\n'.encode('ASCII'))
                        f.flush()
                        break
        else:
            s.close()
            sys.exit(1)

if 'content-length' in dic:
    while True:
        bytes=f.read(int(dic['content-length']))
        sys.stdout.buffer.write(bytes)
        break
elif 'transfer-encoding' in dic:
    while True:
        pocet=f.readline().decode('ASCII')
        if pocet == '0\r\n':
            break
        
        bytes=f.read(int(pocet,16))
        f.readline()
        sys.stdout.buffer.write(bytes)
#blabla
#chungachanga
#for line in f:
#line=line.decode('ASCII')
#print(line)
